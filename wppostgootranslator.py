from googletrans import Translator
from html.parser import HTMLParser

class MyHTMLParser(HTMLParser):

    translator = Translator()

    outputData = ""
    openedTag = ""
    
    def handle_starttag(self, tag, attrs):
        self.openedTag = tag
        self.outputData += "<"
        self.outputData += tag
        
        for name, value in attrs:
            self.outputData += " "
            self.outputData += name
            self.outputData += "=\""
            self.outputData += value
            self.outputData += "\""
        
        self.outputData += ">"

    def handle_endtag(self, tag):
        self.openedTag = ""
        self.outputData += "</"
        self.outputData += tag
        self.outputData += ">"

    def handle_data(self, data):
        if len(data) > 3 and self.openedTag != "code":
            translated = self.translator.translate(data)
            data = translated.text
        
        self.outputData += data
        

inputFile = open("input.txt", 'r')
inputText = inputFile.read()


parser = MyHTMLParser()
parser.feed(inputText)

print(parser.outputData)

outputFile = open("output.txt", 'w')
outputFile.write(parser.outputData)
